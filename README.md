**Main.cpp**
<!-- blank line -->
    the make file generates Main file
                run "./Main simple 4 16" to solve a network of 16 agents in groups of 4 agents.


**SCQL.cpp**
<!-- blank line -->
    Creation of Sparse Cooperative Q-Learning
    Every agent has 2 state variabels:  1) Horizontal cars waiting inline,
                                        2) Vertical cars waiting inline
                                        
    Each variable can take 4 values:    1) 0% full,
                                        2) <25% full, 
                                        3) <50%full, 
                                        4) <100%full
                                        
    Every agent has 1 action which can take 2 values:   1) Horizontal green,
                                                        2) Vertical green
                                                        
    The action of each agent depends on the action of each of its neighbors.
	Rules:
		H State of agent depends on V and H state agent right
		V State of agent right depends on V state of agent left

		V State of agent depends on H and V state agent under
		V State of agent under depends on H state of agent
	
    
    Reward of each agent is defind by:
            R = #Cars passed through - #cars waiting in line that is not green
    
    
**Simulation.cpp**
<!-- blank line -->
    Simulates a traffic light network (grid)
    
    
**Train.cpp**
<!-- blank line -->
    Will solve a given network configuration with sparse cooperative Q-learning.
    Using Epsilon-greedy update.
    
**Test.cpp**
<!-- blank line -->
    Will test the quality of a given sparse cooperative Q-learning policy.
    Using Epsilon-greedy.
    
