#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <algorithm>
#include <iostream>
#include <vector>
#include <numeric>
#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Algorithms/LinearProgramming.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Utils/LP.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <algorithm>
#include <math.h> 



using namespace AIToolbox::Factored;
using namespace AIToolbox::Factored::MDP;
using namespace std;

int MUTATION = 3;

struct Pair {
    std::vector<int> Network;
    float fitness;

    bool operator<( const Pair& rhs ) const
        { return fitness < rhs.fitness; }
};
		



void printV(std::vector<int> vector){
   	printf("The vector:\n");
	std::cout <<"[ ";
	for (auto & i : vector) {
    		std::cout << i <<" ";
	}
	std::cout <<"]\n";		
}

	
// Calculate fitness of every solution in population
std::vector<struct Pair> getFitness(std::vector<struct Pair> pop, int groupsize, int flow){
	for(int i = 0;i < pop.size();i++){
		std::cout <<"Solution "<< i+1<< "\n";
		std::vector<int> network = pop[i].Network;
		pop[i].fitness = train(network, groupsize, flow);
	}
	return pop;
}

//Initialize population
std::vector<struct Pair> createPopulation(int psize, int nsize){
	srand (time(NULL));
	std::vector<struct Pair> pop;
	for (int i = 0 ;i< psize ; ++i){
		std::vector<int> network(nsize);
		std::iota(std::begin(network), std::end(network), 0);
		std::random_shuffle(network.begin(), network.end());
		struct Pair pair;
		pair.Network = network;
		pop.push_back(pair);
	}
	return pop;
}


// new offsprings
std::vector<int> orderCrossover(std::vector<int> parent1, std::vector<int> parent2){
	int size = parent1.size();
	std::vector<int> child(size,-1);
	int start = fmod(rand(),rint(size/10*3));
	int end = start+ round(size/10) + fmod(rand(), (size-start-rint(size/10)));
	for (int i = start ;i< end; ++i){
		child[i] = parent1[i];
		parent2.erase(std::remove(parent2.begin(), parent2.end(), parent1[i]), parent2.end());
	}

	int index = 0;
	for (int i = 0 ;i< child.size(); ++i){
		if(child[i] == -1){
			child[i] = parent2[index];
			index++;
		}
	}
	return child;
}

//mutation
std::vector<int> mutate(std::vector<int> child){
	if(rand()%10 < MUTATION){
		int g1 = rand()%child.size();
		int g2 = rand()%child.size();
		int temp = child[g1];
		child[g1] = child[g2];
		child[g2] = temp;
	}
	return child;
}


// Create offspring from 2 parents
struct Pair getOffspring(struct Pair parent1, struct Pair parent2){
	struct Pair pair;
	std::vector<int> net = orderCrossover(parent1.Network, parent2.Network);
	net = mutate(net);
	pair.Network = net;
	return pair;
}


// each generation crossover best half of population
// Population size, networksize (#agents), groupsize, nr of generations
std::vector<int> trainGenetic(int groupsize, int networksize, int flow, int population, int generations){
	std::vector<struct Pair> pop = createPopulation(population, networksize);	//create population
	struct Pair best;//Best result
	for (int i = 0 ; i < generations; i++){			
		pop = getFitness(pop, groupsize, flow);
		std::sort(pop.begin(), pop.end());// sort population on fitness
		best = (pop[0].fitness> best.fitness)? pop[0] : best;// if best in population better than best seen; save it
		std::vector<struct Pair> newpop;
		while(newpop.size() < pop.size()){//create offsprings from best half of population until same size again 
			struct Pair child = getOffspring(pop[rand()%(pop.size()/2)], pop[rand()%(pop.size()/2)]);
			newpop.push_back(child);
		}
		std::cout <<"Generation "<< i<<" best solution is: "<< best.fitness<<"\n";
		pop = newpop;
	}
	return best.Network;
}



