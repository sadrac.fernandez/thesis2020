namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;
#include <string>
using namespace std;

#include <fstream>
#include <math.h>   
#include <iostream>


int TESTS = 60;		//Amount of tests run during test
int EPISODE = 1000;	//Amount of steps in 1 episode
int TRAINING = 500000;	//Amount of Training periods


void printSolution(std::vector<int> vector){
   	printf("The solution:\n");
	std::cout <<"[ ";
	for (auto & i : vector) {
    		std::cout << i <<" ";
	}
	std::cout <<"]\n";		
}

void printVector(std::vector<long unsigned int> vector){
   	printf("The solution:\n");
	std::cout <<"[ ";
	for (auto & i : vector) {
    		std::cout << i <<" ";
	}
	std::cout <<"]\n";		
}



// get sub vector
vector<int> sub(vector<int> v, int m, int n) {
	auto first = v.begin() + m;
	auto last = v.begin() + n + 1;
	vector<int> vector(first, last);
	return vector;
}


// saves solution to text file
void save_solution(std::vector<int> result){
	std::ofstream outputFile("solution.txt");
	std::copy(result.rbegin(), result.rend(), std::ostream_iterator<int>(outputFile, "\n"));
	outputFile.close();
}

void createFile(string  result){
	remove( "result.txt" );
	std::ofstream outputFile;
	outputFile.imbue(std::locale(""));
	outputFile.open("result.txt", std::ios_base::app); // append instead of overwrite
	outputFile << result << std::endl;
	outputFile.close(); 
}

void append_test(float result){
	std::ofstream outputFile;
	outputFile.imbue(std::locale(""));
	outputFile.open("result.txt", std::ios_base::app); // append instead of overwrite
	outputFile << result << std::endl;
	outputFile.close(); 
}


//loads solution from text file
std::vector<int> load_solution(){
	ifstream inputFile("solution.txt");
	std::vector<int> res;
	std::istream_iterator<int> input(inputFile);
	std::copy(input, std::istream_iterator<int>(), std::back_inserter(res));
	inputFile.close();
	std::reverse(res.begin(),res.end());
	return res;
}


