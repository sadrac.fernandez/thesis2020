namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;

using namespace std;
#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <algorithm>
#include <math.h>       /* sqrt */





//Test QGreedyPolicy
//Takes: previously trained Q greedyPolicies
//Runs simulation few times
float testNetwork(std::vector<fm::QGreedyPolicy> policies, std::vector<struct Scql> groups ,int networksize, int flow){
	//Create Simulation
	Simulation sim(networksize, flow);
	sim.spawnCar(networksize);

	//Amount of groups
	int nr_groups = groups.size();
	float avg = 0;	//average reward
	float spwn = 0;	// average amount of cars spawn
	float avg_steps = 0;
	float avg_waiting = 0;
	
	for(int i = 0; i< TESTS; i++){
		int steps = 0;
		while(steps < EPISODE && !sim.Blocked()){// Every 150 timesteps or until blocked
			steps += 1;
			std::vector<aif::Action> a0;
			// Sample action for each group, from its own greedy policy
			for( int j = 0; j < nr_groups; j++){
				a0.push_back(policies[j].sampleAction(sim.getStates(groups[j].Ids)));
			}
			// Combine actions of groups into one action
			aif::Action action = aif::Action(networksize);	
			for( int j = 0; j < nr_groups; j++){
				for( int l = 0; l < groups[j].Ids.size(); l++){
					action[groups[j].Ids[l]] = a0[j][l];
				}
			}
			
			//Apply action on simulation
			sim.applyActions(action);
			int k = 0;
			while(k<3){// run certain time until new action
				sim.step();		
				k++;
			}
			
		}
		avg_steps += steps;
		avg += sim.reward;
		spwn += sim.spawned;
		avg_waiting += sim.waiting_time;
		if(i == 0){
			sim.displayState();
		}
		sim.reset();

	}
	std::cout << "Avg waiting : "<< avg_waiting/avg << ", Flow: " << spwn/avg_steps << " cars/step, Time until jammed: "<<avg_steps/TESTS << "\n";
	return avg_steps/TESTS;
}
	
	


// Let the network act totally random
void testRandom(int networkSize, int flow){
	//Create Simulation
	Simulation sim(networkSize, flow);
	int i=0;
	float avg = 0;	//average reward
	float spwn = 0;	// average amount of cars spawn
	sim.spawnCar(networkSize);	//Spawn cars
	int avg_steps = 0;
	for(int i = 0; i< TESTS; i++){
		int steps = 0;
		while(steps < EPISODE && !sim.Blocked()){// Until certain timesteps or until blocked
			steps += 1;
			// Random actions of groups into one action
			aif::Action action = aif::Action(networkSize);	
			for( int j = 0; j < networkSize; j++){
				action[j] = rand()%2;
			}
			//Apply action on simulation
			sim.applyActions(action);
			int k = 0;
			while(k<3){// run certain time until new action
				sim.step();		
				k++;
			}
		}
		avg_steps += steps;
		avg += sim.reward;
		spwn += sim.spawned;
		sim.reset();
	}
	std::cout << "Reached dest: "<< avg/spwn*100 << "%, Flow: " << spwn/avg_steps << " cars/step, Time until jammed: "<<avg_steps/TESTS << "\n";
}







