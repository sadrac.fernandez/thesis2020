#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Algorithms/LinearProgramming.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/Policies/EpsilonPolicy.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Utils/LP.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <AIToolbox/Factored/MDP/Algorithms/SparseCooperativeQLearning.hpp>
#include <algorithm>

#include <math.h>       /* pow */
#include <string>
#include "Utils.cpp"
#include "Simulation.cpp"
#include "SCQL.cpp"
#include "Train.cpp"
#include <iostream>


int main (int argc, char *args[]) {
	//Returns best solution of genetic algortihm
	//population size, network size, amount of generations..
	
	if (argc < 5){
		std::cout<< "Not enough parameters passed" << std::endl; 
		return 0;
	}

	// size 1,4,9,16,32
	int groupsize = atoi(args[2]);

	// should always be to power of 2
	int networksize = atoi(args[3]);
	
	//Amount of cars being spawned/ time step
	int flow = atoi(args[4]);

	//pop size and amount of generations
	int population = 10;
	int generations = 10;
	
	std::cout<< "Network of "<<networksize << std::endl; 
	std::cout<< "Agents in groups of "<< groupsize*groupsize << std::endl;
	std::cout<< "Flow through network "<< flow << std::endl; 
	
	string str1 = "Networks size: "+to_string(networksize)+ "\n";
	string str2 = "Group size: "+ to_string(groupsize * groupsize) +"\n";
	string str3 = "Flow: "+ to_string(flow) +"\n";
	createFile( str1+ str2+str3);


	if(std::string(args[1]) == "simple"){
		//Trains & Creates Q greedy policies for each group
		trainSimple(groupsize, groupsize, networksize, flow);
	}
    	return 0;
}








    
