#include <iostream>
#include <string>
#include <math.h>       /* sqrt */
#include<bits/stdc++.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define RESET   "\033[0m"
namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;

using namespace std;

/*
  0
  |
3-+-1
  |
  2
*/

class Car {
	public:
		std::vector<unsigned int> direction;	// list of actions(up, right, down, left)
		int time;	
		int creation;	// timestep the car was spawn

		Car(std::vector<unsigned int> dir, int c) {
			direction = dir;
			time = 0;
			creation = c;
    		}

		// car arrived at its destination
		bool Done(){
			return direction.size()-1 < time;
		}

		void incT(){
			time +=1;
		}

		// direction car will take now
		int getDir(){
			return direction[time];
		}

		// duration of traject  
		int waitingTime(int t){
			return t-(creation+time);			
		}

};


class Intersection {
	public:
		std::vector<queue<Car>> cars; //Amount of cars waiting on each lane
		std::vector<unsigned int> neighbors; //Ids of neighbors
		std::vector<float> lane_size; //Size of that lane
		int action;	//Horizontal green: 0 ; Vertical green: 1
		int passed;
		int waiting = 0;
		

		Intersection(int a) {
			std::queue<Car> w,x,y,z;
			cars = {w,x,y,z};
			action = a;
			lane_size = {20,20,20,20};
			passed = 0;
			waiting = 0;
    		}

		void reset(){
			std::queue<Car> w,x,y,z;
			cars = {w,x,y,z};
			neighbors.clear();
			passed = 0;
			waiting = 0;
		}

		// check if an agent is blocked (atleast 1 lane full)
		bool Blocked(){
			for (int j = 0; j<4; j++){
				if(cars[j].size() >= lane_size[j]-1 ){
					return true;
				}
			}
			return false;
		}

		


		// set list of neighbors ids
		void setNeighbors(std::vector<unsigned int> n) {
			neighbors = n;
    		}

		//add car to lane if not null
		void addCar(Car car, int lane){
			cars[lane].push(car);
		}

		//Get car in lane
		Car getCar(int lane){
			Car c1 = cars[lane].front();
			cars[lane].pop();
			passed++;
			return c1;
		}

		// set action of agent
		void setAction(int a){
			action = a;
		}

		//returns state of lane i of this intersection
		int getState(int i){
			float x = ((float) cars[i].size())/lane_size[i];
			if(x== 0)
				return 0;
			if(x <=0.25)
				return 1;
			if(x <=0.5)
				return 2;
			else
				return 3;
		}

		// Amount of cars agent made wait, H green -> V cars waiting
		int carsWaiting(){
			int temp = 0;
			for (int i = 0 ; i < 4; i++){
				temp += cars[i].size();
			}
			return temp;

		}

		//Returns reward of intersection
		// reward=sum(-state²) 
		//Ex: state(0,0,0,4) is worse than (1,1,1,1)
		int getReward(){
			//if intersection is blocked -100 reward
			if (Blocked())
				return -100;
			int temp = 0;
			for (int j = 0; j<4; j++){
				int state = getState(j);
				temp -= state*state;
			}
			return temp;
 		}
};





class Simulation {        // The class
	public:          // Access specifier
		int nr_of_intersections;      // Attribute
		std::vector<Intersection> intersections;
		int reward = 0;
		int spawned = 0;
		int flow;

		int current_time = 0;	//steps the simulation is running
		int waiting_time = 0;	//time each car has to wait
		

		Simulation(int nr, int _flow) { // Constructor with parameters
			srand(time(NULL));//time(NULL)
			nr_of_intersections = nr;
			flow = _flow;
			for (int i = 0; i < nr; i++) {
				Intersection intersection(1);
				intersection.setNeighbors(createNeighbors(i));	
				intersections.push_back(intersection);
			}
			

    		}
		//check if there are cars in network
		bool Done(){
			return reward == spawned;
		}
		

		//check if a line is full, if yes this means blocked
		bool Blocked(){
			for (auto i : intersections) {
				if (i.Blocked())
					return true;
			}
			return false;
		}


		bool groupBlocked(std::vector<int> group){
			for (int i = 0; i< group.size(); i++) {
				if (intersections[group[i]].Blocked())
					return true;
			}
			return false;
		}

		// calculate ids of neighbors of agent
		std::vector<unsigned int> createNeighbors(int x){
			unsigned int a,b,c,d;
			int temp = sqrt(nr_of_intersections);
			a = (x<temp) ? ((x+(temp/2))%temp) : (x-temp);			//UP
			b = ((x%temp) == temp-1) ? (x+((temp/2)*temp))%(temp*temp) : (x+1);	//RIGHT
			c = (x >= temp*(temp-1)) ? ((x%temp<temp/2) ? (x+(temp/2)) : (x-(temp/2)))  : (x+temp);	//Down	
			d = ((x%temp) == 0) ? ((x<temp*(temp/2)) ? (x+temp*temp/2) : (x-temp*temp/2)) : (x-1); //LEFT
			return {a,b,c,d};
		}

		//Creates random path going through center of grid, using normal distribution
		std::vector<unsigned int> createPath(int start){
			int end; 
			do {
       				end = rand()%nr_of_intersections;
    			} while (end == start);


			int goal = end;
			int current = start;
		 	std::vector<unsigned int> path;
			while(current != goal){
				if(current - goal > sqrt(nr_of_intersections)){
					path.push_back(0);
					current -= sqrt(nr_of_intersections);
				}else if(current - goal < -sqrt(nr_of_intersections)){
					path.push_back(2);
					current += sqrt(nr_of_intersections);
				}else if(current < goal){
					path.push_back(1);
					current += 1;
				}else if(current > goal){
					path.push_back(3);
					current -= 1;
				}
			}
			return path;
		}	

		//Add car to network
		void spawnCar(int nr){
 			for( size_t a = 0 ; a<nr; ++a){
				int start = rand()%nr_of_intersections;
				std::vector<unsigned int> path = createPath(start);
				int lane; 
				do {
       					lane = rand()%4;
    				} while (path[0] == lane);
				intersections[start].addCar(Car(path, current_time), lane);
				spawned++;
			}
		}	


		//let car do action, if car at destination return 1 else 0
		int stepCar(Intersection *i, int lane){
			Car c1 = i->getCar(lane);
			int dir = c1.getDir();
			c1.incT();
			if(!c1.Done()){
				intersections[i->neighbors[dir]].addCar(c1, (dir+2)%4);
				return 0;	
			}
			// time car had to wait
			waiting_time += c1.waitingTime(current_time);
			return 1;
		}

		// Check if lane where car wants to go is not full
		bool checkStep(Intersection *i,int lane){
			if(!i->cars[lane].empty()){// Car waiting at lane of intersection i
				// get intersection where this car is going
				Intersection neigh = intersections[i->neighbors[i->cars[lane].front().getDir()]];
				int n_lane = (i->cars[lane].front().getDir()+2)%2;
				//return true if lane is not full
				return neigh.cars[n_lane].size() < neigh.lane_size[n_lane]-1;
			}else{
				return false;
			}
		}
		
		//Lets cars from 2 lanes do action, if car at destination returns 1
		int doStep(Intersection *i,std::vector<bool> list){
			if(i->action ==0){//Horizontal has green ( lane 1and 3)
				int a = list[0] ? stepCar(i,1) : 0;
				int b = list[1] ? stepCar(i,3) : 0;
				return a+b;
			} else {//Vertical has green ( lane 0 and 2)
				int a = list[0] ? stepCar(i,0) : 0;
				int b = list[1] ? stepCar(i,2) : 0;
				return a+b;
			}
		}

		//check which car can perform its step
		std::vector<bool> prepareStep(Intersection *i){
			if(i->action ==0){//Horizontal has green ( lane 1and 3)
				return {checkStep(i,1),checkStep(i,3)};
			} else {//Vertical has green ( lane 0 and 2)
				return {checkStep(i,0),checkStep(i,2)};
			}
		}

		//perform 1 time step
		//check & perform is splitted to avoid cars doing multiple moves in 1 timestep
		void step(){
			std::vector<std::vector<bool>> list;
			for(Intersection & i : intersections) {// check which cars can do move
				list.push_back(prepareStep(&i));
			}
			int j = 0;
   			for(Intersection & i : intersections) {// perform move
				reward += doStep(&i,list[j]);
				j+=1;
			}	
			spawnCar(rand()%flow);
			current_time++;
	
		}
		
		//set actions( light color) for agents
		void applyActions(std::vector<long unsigned int> actions){
			int j = 0;
			for (Intersection & i : intersections) {
				i.setAction(actions[j]);
				j++;
			}
		}

		// get state variables from group of agents (4 states per agent)
		std::vector<long unsigned> getStates(std::vector<int> group){
			std::vector<long unsigned> state;
			for (int i : group) {
				for( int j = 0 ;j<4;j++){ 
					state.push_back(intersections[i].getState(j));
				}
			}
			return state;
		}

		int countCars(){
			int temp = 0;
			for (Intersection & i : intersections) {
				temp += i.cars[0].size()+ i.cars[1].size()+i.cars[2].size()+i.cars[3].size();
			}
			return temp;
		}


		// get state variables from group of agents
		aif::Rewards getReward(std::vector<int> group){
			aif::Rewards rewards(group.size());
			for (int i = 0 ; i< group.size(); i++) {
				int temp = intersections[i].getReward();
				rewards(i) = temp;
			}
			return rewards;
		}

		void reset(){
			reward = 0;
			spawned = 0;
			for (int i = 0; i < nr_of_intersections; i++) {
				intersections[i].reset();
			}
			spawnCar(nr_of_intersections);
			
		}

		
		void displayHeatmap(){
			std::cout <<"Heatmap----------------------------------------\n";
			int temp = sqrt(nr_of_intersections);
			for (int i = 0; i < temp; i++) {
				std::cout <<"\n";
				for (int j = 0; j < temp; j++) {
					std::cout << intersections[j+temp*i].passed;
					std::cout <<"\t";
				}
				std::cout <<"\n";
			}
			std::cout <<"Score is: "<< reward <<"\n";
			std::cout <<"----------------------------------------\n";
		}

// show network amount of cars waiting at each light, and current action of light
		void displayState(){
			std::cout <<"----------------------------------------\n";
			int temp = sqrt(nr_of_intersections);
			string COLOR = RESET;
			for (int i = 0; i < temp; i++) {
				for (int k = 0; k < 3; k++) {
					std::cout <<"\n";
					for (int j = 0; j < temp; j++) {
						if( k == 1){
							(intersections[j+temp*i].action ==0)? (COLOR = GREEN):(COLOR = RED);
							std::cout << COLOR << intersections[j+temp*i].cars[3].size();
							std::cout <<" ";
							std::cout << intersections[j+temp*i].cars[1].size() <<RESET;
						}else{
							(intersections[j+temp*i].action !=0)? (COLOR = GREEN):(COLOR = RED);
							std::cout << COLOR<<" ";
							std::cout << intersections[j+temp*i].cars[k].size();
							std::cout <<" " <<RESET;
						}
						std::cout <<"\t";
					}
						
				}
				std::cout <<"\n";
			}
			std::cout <<"Score is: "<< reward <<"\n";
			std::cout <<"----------------------------------------\n";
		}

};







