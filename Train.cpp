namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;

using namespace std;
#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <algorithm>
#include <math.h>       /* sqrt */

#include "Test.cpp"

//Train SparseCooperativeQlearning
//Takes: agent network, groupsize, amount of timesteps training
//Return QgreedyPolicy
float train(std::vector<struct Scql> groups ,int networksize, int flow){
	//Create Simulation
	Simulation sim(networksize, flow);
	sim.spawnCar(networksize);

	//Learning rate
	double learning_rate= 0.3;
	double epsilon = 2;
	//group that is currently trained.
	int active = 0;
	//Group to train
	int nr_groups = groups.size();
	std::vector<aif::Action> a0;

	//Keep how many times each group caused a block
	std::vector<int> blocks(nr_groups, 0); 

	//Create Q learners
	std::vector<fm::SparseCooperativeQLearning> learners;
	for( int i = 0; i < nr_groups; i++){
		learners.push_back(createSCQL(groups[i], learning_rate));
		a0.push_back(aif::Action (groups[i].Ids.size(), 0));
	}

	int finished = 0;
	int episodes = 0;
	for ( int i = 0 ; i < TRAINING ; i++){
		int steps = 0;
		while( (steps < EPISODE) && (sim.Blocked() == false) ){
			//state at t=0
			std::vector<aif::State> s0;

			// Get state variables of each group
			for( int j = 0; j < nr_groups; j++){
				s0.push_back(sim.getStates(groups[j].Ids));
			}
			// Combine actions of groups into one action
			aif::Action action = aif::Action(networksize);
		
			//decide if explore
			bool explore = rand()%10>epsilon;
			for( int j = 0; j < nr_groups; j++){
				for( int l = 0; l < groups[j].Ids.size(); l++){
					if(explore){
						action[groups[j].Ids[l]] = a0[j][l];
					}else{// sometimes do random action, for diversification
						action[groups[j].Ids[l]] = rand()%2;
					}
				}
			}

			//Apply action on simulation
			sim.applyActions(action);

			int k = 0;
			while(k<3){// run certain timesteps until new action
				sim.step();		
				k++;
			}
			
			// update Q of active group
			a0[active] = learners[active].stepUpdateQ(s0[active], a0[active], sim.getStates(groups[active].Ids), sim.getReward(groups[active].Ids));
			
			//get actions of other groups
			for( int j = 0; j < nr_groups && j != active; j++){
				fm::QGreedyPolicy policy(learners[j].getS(),learners[j].getA(),learners[j].getQFunctionRules());
				a0[j] = policy.sampleAction(sim.getStates(groups[j].Ids));
				//a0[j] = learners[j].stepUpdateQ(s0[j], a0[j], sim.getStates(groups[j].Ids), sim.getReward(groups[j].Ids));
			}
			steps++;
		}
		episodes++;

		//save which groups blocked.
		for( int l = 0; l < nr_groups; l++){
			if(sim.groupBlocked(groups[l].Ids)){
				blocks[l]++;
			}
		}

		//Test solution quality
		if( (i+1)%(nr_groups*100)==0){
			std::vector<fm::QGreedyPolicy> policies;
			learning_rate = std::max(0.01, learning_rate*=0.95);
			for( int l = 0; l < nr_groups; l++){
				learners[l].setLearningRate(learning_rate);
				fm::QGreedyPolicy policy(learners[l].getS(),learners[l].getA(),learners[l].getQFunctionRules());
				policies.push_back(policy);
			}
			float res = testNetwork(policies, groups, networksize, flow);
			append_test(res);
		}
		
		//check if current active group blocks
		if(sim.groupBlocked(groups[active].Ids) == false){
			finished++;
		}else{
			finished =0;
		}

		//When group trained enough pick new group
		if(episodes > 100){
			bool explore = rand()%10>2;
			printSolution(blocks);
			if (explore){//with certain probabilty pick group that caused system to block
				active = std::max_element(blocks.begin(),blocks.end()) - blocks.begin();
			}else{//with certain probabilty pick random group to train
				active =  rand()%groups.size();
			}
			std::fill (blocks.begin(),blocks.end(),0);
			finished = 0;
			episodes = 0;

		}
		sim.reset();
	}
	
	//After training return quality of solution
	std::vector<fm::QGreedyPolicy> policies;
	for( int l = 0; l < nr_groups; l++){
		fm::QGreedyPolicy policy(learners[l].getS(),learners[l].getA(),learners[l].getQFunctionRules());
		policies.push_back(policy);
	}
	return testNetwork(policies, groups, networksize, flow);
}





//Splits original problem into simple blocks
float trainSimple(int h, int v, int networksize, int flow){
	int x = sqrt(networksize);
	int groupsize = h*v;
	std::vector<struct Scql> groups;

	for( int r = 0; r < networksize/groupsize; r++){
		int temp = (r%(x/h))*h;// move ids to right(add h) 
		temp += (r/(x/v))*(v*x); // move ids down (add v)
		std::vector<int> ids;
		for( int i = 0; i < v; i++){
			for( int j = 0; j < h; j++){
				int id = j+i*x+temp;
				ids.push_back(id);
			}
		}
		Scql group = {h,v,ids};
		groups.push_back(group);
	}
	return train(groups, networksize, flow);
}














