#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Algorithms/SparseCooperativeQLearning.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <algorithm>
#include <cmath>

#include <math.h>       /* sqrt */
#include <iostream>
namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;

struct Scql {
	int H;
	int V;
	std::vector<int> Ids;
};

fm::SparseCooperativeQLearning createSCQL(struct Scql group, double alpha){

	
	int h = group.H; // Amount of agents horizontally
	int v = group.V; // Amount of agents vertically

	int agents = group.Ids.size();
	//Every agent has 4 state variables.(Every lane) 
	aif::State S(agents * 4);
	//Each of them can assume 4 different values.
        std::fill(std::begin(S), std::end(S), 4);

	//Every agent has 1 action.
        aif::Action A(agents);
	//Which can assume 2 different values.
        std::fill(std::begin(A), std::end(A), 2);

	//Parameters
	const double discount = 0.9;
	fm::SparseCooperativeQLearning solver(S, A, discount, alpha);
	double initialvalue = 1;

	 
	//Creation of rules
	for (long unsigned int x = 0; x < agents;x++){
		//groups of size 1
		//4state variables can each take values between 0 and 3
		aif::PartialFactorsEnumerator s(aif::Factors(4, 4));
		for (; s.isValid(); s.advance()) {
			//1action variables can take values 0 or 1
			aif::PartialFactorsEnumerator a(aif::Factors(1, 2));
			for (; a.isValid(); a.advance()) {
				// Lane 0,1,2,3 of agent x depend on action of agent x
				double value = -pow(s->second[0],2)-pow(s->second[1],2)-pow(s->second[2],2)-pow(s->second[3],2);
				const fm::QFunctionRule r1{{{4*x, 4*x+1, 4*x+2, 4*x+3},s->second}, {{x},a->second}, initialvalue};
				solver.insertRule(r1);
			}
		}


		if(agents > 1){//Larger groups
			aif::PartialFactorsEnumerator s(aif::Factors(3, 4));
			for (; s.isValid(); s.advance()) {
				//2action variables can each take values 0 or 1
				aif::PartialFactorsEnumerator a(aif::Factors(2, 2));
				for (; a.isValid(); a.advance()) {	
					//agent above x
					if(x>h-1){
						//Lane 1,2,3 of agent x send cars to lane 2 of agent x-h
						std::vector<unsigned long int> sttt = {3,s->second[0], s->second[1],s->second[2]};
						const fm::QFunctionRule r1{{{4*(x-h)+2, 4*x+1, 4*x+2, 4*x+3},sttt}, {{x-h, x},a->second}, initialvalue};
						solver.insertRule(r1);
					}
					//agent right of x
					if((x+1)%h !=0){
						//Lane 0,2,3 of agent x send cars to lane 3 of agent x+1
						std::vector<unsigned long int> sttt = {s->second[0], s->second[1],s->second[2], 3};
						const fm::QFunctionRule r1{{{4*x,4*x+2,4*x+3,4*(x+1)+3},sttt}, {{x, x+1},a->second}, initialvalue};
						solver.insertRule(r1);
					}
					//agent under x
					if(x<h*(v-1)){
						//Lane 0,1,3 of agent x send cars to lane 0 of agent x+h
						std::vector<unsigned long int> sttt = {s->second[0], s->second[1],s->second[2], 3};
						const fm::QFunctionRule r1{{{4*x,4*x+1, 4*x+3,4*(x+h)},sttt}, {{x, x+h},a->second}, initialvalue};
						solver.insertRule(r1);
					}

					//agent left of x		
					if(x%h != 0){ 
						//Lane 0,1,2 of agent x send cars to lane 1 of agent x-1
						std::vector<unsigned long int> sttt = {3,s->second[0], s->second[1],s->second[2]};
						const fm::QFunctionRule r1{{{4*(x-1)+1, 4*x, 4*x+1,  4*x+2},sttt}, {{x-1, x},a->second}, initialvalue};
						solver.insertRule(r1);
					}
				}
			}
		}
	}

	return solver;
	}



